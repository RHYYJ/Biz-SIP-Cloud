package com.bizmda.bizsip.sample.serveradaptor.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages={"cn.hutool.extra.spring","com.bizmda.bizsip.sample"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleServerApplication.class, args);
    }
}