package com.bizmda.bizsip.sample.serveradaptor.server.controller;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.serveradaptor.protocol.java.JavaProtocolInterface;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 史正烨
 */
@Slf4j
public class EchoServer implements JavaProtocolInterface {
    @Override
    public byte[] process(byte[] inMessage) throws BizException {
        log.debug("EchoServer传入消息:[{}]", BizUtils.getString(inMessage));
        return inMessage;
    }
}
