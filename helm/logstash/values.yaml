# Default values for logstash.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}

logstashConfig: 
  logstash.yml: |
    node.name: logstast-node1
    http.host: 0.0.0.0
    path.config: /usr/share/logstash/config/logstash.conf
    log.level: debug
  logstash.conf: |
    input {
      beats {
        port => 5044
      }
    }

    filter {
      if [type] == "syslog" {
        grok {
          match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
          add_field => [ "received_at", "%{@timestamp}" ]
          add_field => [ "received_from", "%{host}" ]
        }
        syslog_pri { }
        date {
          match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
        }
      }
      if [fields][docType] == "sys-log" {
        grok {
          patterns_dir => ["/opt/logstash/patterns"]
          match => { "message" => "\[%{NOTSPACE:appName}:%{IP:serverIp}:%{NOTSPACE:serverPort}\] %{TIMESTAMP_ISO8601:logTime} %{LOGLEVEL:logLevel} %{WORD:pid} \[%{MYAPPNAME:traceId}\] \[%{MYTHREADNAME:threadName}\] %{NOTSPACE:classname} %{GREEDYDATA:message}" }
          overwrite => ["message"]
        }
        date {
          match => ["logTime","yyyy-MM-dd HH:mm:ss.SSS Z"]
        }
        date {
          match => ["logTime","yyyy-MM-dd HH:mm:ss.SSS"]
          target => "timestamp"
          locale => "en"
          timezone => "+08:00"
        }
        mutate {
          remove_field => "logTime"
          remove_field => "@version"
          remove_field => "host"
          remove_field => "offset"
        }
      }
      if [fields][docType] == "point-log" {
        grok {
          patterns_dir => ["/opt/logstash/patterns"]
          match => {
            "message" => "%{TIMESTAMP_ISO8601:logTime}\|%{MYAPPNAME:appName}\|%{WORD:resouceid}\|%{MYAPPNAME:type}\|%{GREEDYDATA:object}"
          }
        }
        kv {
            source => "object"
            field_split => "&"
            value_split => "="
        }
        date {
          match => ["logTime","yyyy-MM-dd HH:mm:ss.SSS Z"]
        }
        date {
          match => ["logTime","yyyy-MM-dd HH:mm:ss.SSS"]
          target => "timestamp"
          locale => "en"
          timezone => "+08:00"
        }
        mutate {
          remove_field => "message"
          remove_field => "logTime"
          remove_field => "@version"
          remove_field => "host"
          remove_field => "offset"
        }
      }
      if [fields][docType] == "mysqlslowlogs" {
        grok {
            match => [
              "message", "^#\s+User@Host:\s+%{USER:user}\[[^\]]+\]\s+@\s+(?:(?<clienthost>\S*) )?\[(?:%{IP:clientip})?\]\s+Id:\s+%{NUMBER:id}\n# Query_time: %{NUMBER:query_time}\s+Lock_time: %{NUMBER:lock_time}\s+Rows_sent: %{NUMBER:rows_sent}\s+Rows_examined: %{NUMBER:rows_examined}\nuse\s(?<dbname>\w+);\nSET\s+timestamp=%{NUMBER:timestamp_mysql};\n(?<query_str>[\s\S]*)",
              "message", "^#\s+User@Host:\s+%{USER:user}\[[^\]]+\]\s+@\s+(?:(?<clienthost>\S*) )?\[(?:%{IP:clientip})?\]\s+Id:\s+%{NUMBER:id}\n# Query_time: %{NUMBER:query_time}\s+Lock_time: %{NUMBER:lock_time}\s+Rows_sent: %{NUMBER:rows_sent}\s+Rows_examined: %{NUMBER:rows_examined}\nSET\s+timestamp=%{NUMBER:timestamp_mysql};\n(?<query_str>[\s\S]*)",
              "message", "^#\s+User@Host:\s+%{USER:user}\[[^\]]+\]\s+@\s+(?:(?<clienthost>\S*) )?\[(?:%{IP:clientip})?\]\n# Query_time: %{NUMBER:query_time}\s+Lock_time: %{NUMBER:lock_time}\s+Rows_sent: %{NUMBER:rows_sent}\s+Rows_examined: %{NUMBER:rows_examined}\nuse\s(?<dbname>\w+);\nSET\s+timestamp=%{NUMBER:timestamp_mysql};\n(?<query_str>[\s\S]*)",
              "message", "^#\s+User@Host:\s+%{USER:user}\[[^\]]+\]\s+@\s+(?:(?<clienthost>\S*) )?\[(?:%{IP:clientip})?\]\n# Query_time: %{NUMBER:query_time}\s+Lock_time: %{NUMBER:lock_time}\s+Rows_sent: %{NUMBER:rows_sent}\s+Rows_examined: %{NUMBER:rows_examined}\nSET\s+timestamp=%{NUMBER:timestamp_mysql};\n(?<query_str>[\s\S]*)"
            ]
        }
        date {
          match => ["timestamp_mysql","yyyy-MM-dd HH:mm:ss.SSS","UNIX"]
        }
        date {
          match => ["timestamp_mysql","yyyy-MM-dd HH:mm:ss.SSS","UNIX"]
          target => "timestamp"
        }
        mutate {
          convert => ["query_time", "float"]
          convert => ["lock_time", "float"]
          convert => ["rows_sent", "integer"]
          convert => ["rows_examined", "integer"]
          remove_field => "message"
          remove_field => "timestamp_mysql"
          remove_field => "@version"
        }
      }
    }

    output {
      if [fields][docType] == "sys-log" {
        elasticsearch {
          hosts => ["http://elasticsearch:9200"]
          user => "elastic"
          password => "bizsip"
          index => "sys-log-%{+YYYY.MM.dd}"
        }
      }
      if [fields][docType] == "point-log" {
        elasticsearch {
          hosts => ["http://elasticsearch:9200"]
          user => "elastic"
          password => "bizsip"
          index => "point-log-%{+YYYY.MM.dd}"
          routing => "%{type}"
        }
      }
      if [fields][docType] == "mysqlslowlogs" {
        elasticsearch {
          hosts => ["http://elasticsearch:9200"]
          user => "elastic"
          password => "bizsip"
          index => "mysql-slowlog-%{+YYYY.MM.dd}"
        }
      }
    }

# Allows you to add any pipeline files in /usr/share/logstash/pipeline/
logstashPatterns:
  java: |
    # user-center
    MYAPPNAME ([0-9a-zA-Z_-]*)
    # RMI TCP Connection(2)-127.0.0.1
    MYTHREADNAME ([0-9a-zA-Z._-]|\(|\)|\s)*