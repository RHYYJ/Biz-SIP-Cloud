echo "build integrator..."
cp integrator/src/main/docker/Dockerfile integrator/target
docker build -t dockerhub.qingcloud.com/bizsip_istio/bizsip-integrator integrator/target
docker push dockerhub.qingcloud.com/bizsip_istio/bizsip-integrator
#echo "build dynamic-config..."
#cp dynamic-config/src/main/docker/Dockerfile dynamic-config/target
#docker build -t dockerhub.qingcloud.com/bizsip_istio/bizsip-dynamic-config dynamic-config/target
#docker push dockerhub.qingcloud.com/bizsip_istio/bizsip-dynamic-config
#echo "build sample-client..."
#cp sample/sample-client/src/main/docker/Dockerfile sample/sample-client/target
#docker build -t dockerhub.qingcloud.com/bizsip_istio/bizsip-sample-client sample/sample-client/target
#docker push dockerhub.qingcloud.com/bizsip_istio/bizsip-sample-client
echo "build sample-server..."
cp sample/sample-server/src/main/docker/Dockerfile sample/sample-server/target
docker build -t dockerhub.qingcloud.com/bizsip_istio/bizsip-sample-server sample/sample-server/target
docker push dockerhub.qingcloud.com/bizsip_istio/bizsip-sample-server
#echo "build netty-client-adaptor..."
#cp client-adaptor/netty-client-adaptor/src/main/docker/Dockerfile client-adaptor/netty-client-adaptor/target
#docker build -t dockerhub.qingcloud.com/bizsip_istio/bizsip-netty-client-adaptor client-adaptor/netty-client-adaptor/target
#docker push dockerhub.qingcloud.com/bizsip_istio/bizsip-netty-client-adaptor
